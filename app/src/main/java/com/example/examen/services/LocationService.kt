package com.example.examen.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.location.Location
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.examen.MainActivity
import com.example.examen.Models
import com.example.examen.utils.AppDatabase
import com.example.examen.utils.Utils
import com.google.android.gms.location.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import android.media.RingtoneManager
import android.location.Criteria
import android.widget.Toast

class LocationService : Service() {
    override fun onBind(p0: Intent?): IBinder? {
        Log.i(TAG, "in onBind()")
        stopForeground(true)
        mChangingConfiguration = false
        return mBinder
    }

    lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var mLocationCallback: LocationCallback
    lateinit var mNotificationManager: NotificationManager
    lateinit var mLocationRequest: LocationRequest
    lateinit var mLocation: Location
    lateinit var database: AppDatabase

    private val PACKAGE_NAME = LocationService::class.java!!.`package`
    val TAG = LocationService::class.java!!.simpleName
    val CHANNEL_ID = "channel_01"
    val ACTION_BROADCAST = "$PACKAGE_NAME.broadcast"
    val EXTRA_LOCATION = "$PACKAGE_NAME.location"
    val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.started_from_notification"

    private val NOTIFICATION_ID = 12345678

    private var mChangingConfiguration = false

    private val mBinder = LocalBinder()
    var mServiceHandler: Handler? = null

    override fun onCreate() {
        super.onCreate()

        database = AppDatabase.getInstance(this)

        mFusedLocationClient =
            LocationServices.getFusedLocationProviderClient(this@LocationService)
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)

                locationResult?.lastLocation?.let { onNewLocation(it) }
            }
        }

        createLocationRequest()
        getLastLocation()

        val handlerThread = HandlerThread(TAG)
        handlerThread.start()
        mServiceHandler = Handler(handlerThread.looper)
        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(com.example.examen.R.string.app_name)
            val mChannel = NotificationChannel(
                CHANNEL_ID,
                name,
                NotificationManager.IMPORTANCE_DEFAULT
            )

            mNotificationManager.createNotificationChannel(mChannel)
        }
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.smallestDisplacement = 0.1F
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    inner class LocalBinder : Binder() {
        val service: LocationService
            get() = this@LocationService
    }

    fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (javaClass.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }

        return false
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.i(TAG, "Service started")
        var startedFromNotification = false
        try {
            startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION, false)
        } catch (e: Exception) {

        }

        if (startedFromNotification) {
            removeLocationUpdates()
            stopSelf()
        }

        return Service.START_NOT_STICKY
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mChangingConfiguration = true
    }

    override fun onRebind(intent: Intent) {
        Log.i(TAG, "in onRebind()")
        stopForeground(true)
        mChangingConfiguration = false
        super.onRebind(intent)
    }

    override fun onUnbind(intent: Intent): Boolean {
        Log.i(TAG, "Last client unbound from service")
        if (!mChangingConfiguration && Utils().requestingLocationUpdates(this)) {
            Log.i(TAG, "Starting foreground service")

            startForeground(NOTIFICATION_ID, getNotification())
        }
        return true
    }

    override fun onDestroy() {
        if (mServiceHandler != null)
            mServiceHandler?.removeCallbacksAndMessages(null)
    }

    fun requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates")
        Utils().setRequestingLocationUpdates(this, true)
        startService(Intent(applicationContext, LocationService::class.java))
        try {
            mFusedLocationClient.requestLocationUpdates(
                mLocationRequest,
                mLocationCallback,
                Looper.myLooper()
            )

        } catch (unlikely: SecurityException) {
            Utils().setRequestingLocationUpdates(this, false)
            Log.e(TAG, "Lost location permission. Could not request updates. $unlikely")
        }

    }

    fun removeLocationUpdates() {
        Log.i(TAG, "Removing location updates")
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback)
            Utils().setRequestingLocationUpdates(this, false)
            stopSelf()
        } catch (unlikely: SecurityException) {
            Utils().setRequestingLocationUpdates(this, true)
            Log.e(TAG, "Lost location permission. Could not remove updates. $unlikely")
        }

    }

    private fun getNotification(): Notification {
        val intent = Intent(this, LocationService::class.java)

        val text = Utils().getLocationText(mLocation)
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true)

        val activityPendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, MainActivity::class.java), 0
        )

        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val builder = NotificationCompat.Builder(this)
            .addAction(com.example.examen.R.drawable.ic_android_black_24dp, "Regresar", activityPendingIntent)
            .setContentText(text)
            .setContentTitle(Utils().getLocationTitle(this))
            .setOngoing(true)
            .setPriority(Notification.PRIORITY_HIGH)
            .setSmallIcon(com.example.examen.R.mipmap.ic_launcher)
            .setTicker(text)
            .setDefaults(Notification.DEFAULT_LIGHTS or Notification.DEFAULT_SOUND)
            .setVibrate(longArrayOf(0L))
            .setSound(uri)
            .setWhen(System.currentTimeMillis())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID)
        }

        return builder.build()
    }

    private fun getLastLocation() {
        try {
            mFusedLocationClient.lastLocation.addOnCompleteListener { task ->
                if (task.isSuccessful && task.result != null) {
                    mLocation = task.result!!
                } else {
                    Log.w(TAG, "Failed to get location.")
                }
            }
        } catch (unlikely: SecurityException) {
            Log.e(TAG, "Lost location permission.$unlikely")
        }
    }

    private fun onNewLocation(location: Location) {
        Log.i(TAG, "New location: $location")

        mLocation = location

        database.phoneDao().loadLast()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : DisposableSingleObserver<Models.Phone>() {
                override fun onSuccess(t: Models.Phone) {
                    val intent = Intent(ACTION_BROADCAST)
                    intent.putExtra(EXTRA_LOCATION, location)
                    LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)

                    Utils().sendSMS(this@LocationService, t?.telefono, "Estoy en la posición ${location.latitude},${location.longitude}")
                    Toast.makeText(this@LocationService, "Enviando posición", Toast.LENGTH_SHORT).show()
                }

                override fun onError(e: Throwable) {
                    Log.e("Error", "Error ${e.message}")
                }
            }
            )


        if (serviceIsRunningInForeground(this)) {
            mNotificationManager.notify(NOTIFICATION_ID, getNotification())
        }
    }
}