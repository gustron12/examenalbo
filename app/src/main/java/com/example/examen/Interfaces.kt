package com.example.examen

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class Interfaces {

    @Dao
    public interface PhoneDao {
        @Query("SELECT * FROM phone")
        fun loadAll(): Flowable<List<Models.Phone>>

        @Query("SELECT * FROM phone ORDER BY fecha DESC LIMIT 1")
        fun loadLast(): Single<Models.Phone>

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        fun insertAll(places: List<Models.Phone>): Completable

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        fun insert(place: Models.Phone): Completable

        @Query("select * from phone where telefono = :telefono")
        fun load(telefono: String): LiveData<Models.Phone>

        @Delete
        fun delete(place: Models.Phone)

        @Query("DELETE FROM phone")
        fun deleteAll(): Completable
    }
}