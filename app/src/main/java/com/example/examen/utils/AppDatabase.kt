package com.example.examen.utils

import android.content.Context

import androidx.room.Room
import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.examen.Interfaces
import com.example.examen.Models

@Database(entities = [Models.Phone::class], version = 1)
public abstract class AppDatabase : RoomDatabase() {
    abstract fun phoneDao(): Interfaces.PhoneDao

    companion object {
        var DATABASE_NAME: String = "Examen"

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase = INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context,
                AppDatabase::class.java, DATABASE_NAME
            ).fallbackToDestructiveMigration().build()
    }
}