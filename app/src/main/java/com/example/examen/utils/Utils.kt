package com.example.examen.utils

import android.content.Context
import android.location.Location
import android.preference.PreferenceManager
import android.telephony.SmsManager
import android.util.Log
import android.widget.Toast
import com.example.examen.R
import java.text.DateFormat
import java.util.*

class Utils {

    private val KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates"

    fun sendSMS(context: Context, phoneNo: String?, msg: String?) {
        try {
            val smsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phoneNo, null, msg, null, null)
            Toast.makeText(context, "Message Sent", Toast.LENGTH_LONG).show()
            Log.e("TAg", "TAG: Enviado")
        } catch (ex: Exception) {
            Toast.makeText(context, ex.message.toString(), Toast.LENGTH_LONG).show()
            Log.e("TAg", "TAG: No enviado")
        }
    }

    fun requestingLocationUpdates(context: Context): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false)
    }

    fun setRequestingLocationUpdates(context: Context, requestingLocationUpdates: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
            .apply()
    }

    fun getLocationText(location: Location?): String {
        return if (location == null)
            "Unknown location"
        else
            "Posición actual: ${location.latitude}, ${location.longitude}"
    }

    fun getLocationTitle(context: Context): String {
        return context.getString(
            R.string.location_updated,
            Date().toString()
        )
    }
}