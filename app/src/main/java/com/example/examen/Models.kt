package com.example.examen

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

class Models {

    @Entity(tableName = "phone")
    data class Phone(
        @PrimaryKey var telefono: String,
        @ColumnInfo(name = "nombre") var nombre: String?,
        @ColumnInfo(name = "fecha") var fecha: String?
    )
}